aws dynamodb create-table \
--table-name TB_001 \
--attribute-definitions AttributeName=COD_ATTR_001,AttributeType=N AttributeName=COD_ATTR_002,AttributeType=S \
--key-schema AttributeName=COD_ATTR_001,KeyType=HASH AttributeName=COD_ATTR_002,KeyType=RANGE \
--provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 \
--endpoint-url http://localhost:8000



aws dynamodb scan --table-name TB_001 --endpoint-url http://localhost:8000

aws dynamodb delete-table --table-name TB_001 --endpoint-url http://localhost:8000
aws dynamodb list-tables --endpoint-url http://localhost:8000 

# aws dynamodb create-table \
# --table-name TB_001 \
# --attribute-definitions AttributeName=COD_ATTR_001,AttributeType=N AttributeName=COD_ATTR_002,AttributeType=S \
# AttributeName=NUM_ATTR_003,AttributeType=N AttributeName=NUM_ATTR_004,AttributeType=N \
# AttributeName=NUM_ATTR_005,AttributeType=N AttributeName=VLR_ATTR_006,AttributeType=N AttributeName=VLR_ATTR_007,AttributeType=N \
# AttributeName=VLR_ATTR_008,AttributeType=N AttributeName=NUM_ATTR_009,AttributeType=N AttributeName=DES_ATTR_010,AttributeType=S \
# --key-schema AttributeName=COD_ATTR_001,KeyType=HASH AttributeName=COD_ATTR_002,KeyType=RANGE \
# --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 \
# --endpoint-url http://localhost:8000