#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import os, sys, csv
from common_lib.common_base import get_input_parameter_value, valid_mandatory_parameters, print_sql_durations
from common_lib.common_error import BadUserInputError
from common_lib.process_bar import ProcessBar
from common_lib.enum_csv_lines_load_type import CsvLinesLoadType
from common_lib.mysql_helper import MySqlHelper
from common_lib.log_helper import LogHelper
from datetime import datetime
from os.path import dirname, abspath
from dotenv import load_dotenv
import boto3
from decimal import Decimal

AWS_REGION="sa-east-1"
path = dirname(abspath(__file__)) + '/.env'
load_dotenv(path)


TABLE_NAME = "TB_001"
CSV_LINE_SEP = '\r\n'

log_helper = None
def load_csv_to_table(csv_path: str, number_of_rows_by_insert: int)-> None: 

    if not os.path.exists(csv_path):
        raise BadUserInputError(
            f"ERROR: The file {csv_path} doesn't exists")

    start_date_time = datetime.now()
    log_helper.print_log(f"Start date/time {start_date_time}") 
    row_count  = 0

    with open(csv_path) as fp:
        for _ in fp:
            row_count += 1
            
    with open(csv_path, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')
        number_of_lines_read = 0
        total_number_of_lines_read = 0
        rows_to_insert = []
        process_bar=ProcessBar(total_iteration=row_count,
                                start_interator=1,
                                prefix='Progress:',
                                suffix='Complete',
                                length=50)

        process_bar.start()

        insert_parts_count = 0
        for row in spamreader:
            total_number_of_lines_read = total_number_of_lines_read + 1
            number_of_lines_read = number_of_lines_read + 1
            rows_to_insert.append({
                                    "COD_ATTR_001": int(row[0]),
                                    "COD_ATTR_002": row[1],
                                    "NUM_ATTR_003": int(row[2]),
                                    "NUM_ATTR_004": int(row[3]),
                                    "NUM_ATTR_005": int(row[4]),
                                    "VLR_ATTR_006": Decimal (row[5]),
                                    "VLR_ATTR_007": Decimal (row[6]),
                                    "VLR_ATTR_008": Decimal (row[7]),
                                    "VLR_ATTR_009": Decimal (row[8]),
                                    "DES_ATTR_010": row[9]
                                })
            
            if number_of_lines_read == number_of_rows_by_insert:
                insert_parts_count = insert_parts_count+1
                
                db = boto3.resource('dynamodb', 
                                    region_name=AWS_REGION, 
                                    aws_access_key_id='demmy',
                                    aws_secret_access_key='demmy',
                                    endpoint_url="http://localhost:8000")
                table = db.Table('TB_001')  
            
                with table.batch_writer() as batch:
                    for row_to_insert in rows_to_insert:
                        batch.put_item(Item=row_to_insert)
                
                rows_to_insert.clear()
                number_of_lines_read = 0
                process_bar.print_progress_bar(iteration=total_number_of_lines_read)
        
    log_helper.print_log("")
    log_helper.print_log(f"Time spent by insert parts of {number_of_rows_by_insert} row(s)")

    end_date_time = datetime.now() 
    log_helper.print_log(f"Finish date/time {end_date_time}") 
    log_helper.print_log(f"Total time = {end_date_time - start_date_time}")

def read_line(keys:dict, csvfile, index):
    line = csvfile.readline().rstrip(CSV_LINE_SEP)
    if line != '':
        line_lenght = len(line)
        keys[line.split(';',2)[1]] = (index, line_lenght)
    
    index = 0 if line == '' else (index + line_lenght + len(CSV_LINE_SEP))
    return index




def start(argv):
    global log_helper
    if (('-h' in argv) or ('-?' in argv)):
        log_helper.print_log("""
        python3 csv_lines_to_mysql_import.py -p CSV_FULL_PATH [-t LOAD_LINES_TYPE] [-n LINES_BY_INSERT] [-h|-?]
        Program to load a csv to a mysql table
        Parameters:
            -p CSV_FULL_PATH  --> mandatory, if not specified will generate csv on current path
            -n LINES_BY_INSERT --> optional, number of lines by insert, if not specified, will be 1 line per insert
            -h or -? help
        """)
    
    elif not valid_mandatory_parameters(argv, ['-p']):
        raise BadUserInputError(
            "Input error. To run, call as python3 csv_lines_to_mysql_import.py -p CSV_FULL_PATH [-t LOAD_LINES_TYPE] [-n LINES_BY_INSERT] [-h|-?]")

    else:
        csv_path:str = get_input_parameter_value(argv,'-p')
        number_of_rows_by_insert_param = get_input_parameter_value(argv,'-n')
        number_of_rows_by_insert = 1 if number_of_rows_by_insert_param is None else int(number_of_rows_by_insert_param)
    
    
    
    log_helper = LogHelper(f"{datetime.now().strftime('%Y%m%d%H%M%S')}_{os.path.basename(csv_path).replace('.csv','')}_by_{number_of_rows_by_insert}_csv_to_table.log")
    load_csv_to_table(csv_path, number_of_rows_by_insert)



start(sys.argv)
